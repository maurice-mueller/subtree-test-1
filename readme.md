# Subtree Test

A simple test for git sub trees.

This is the main repo.

## Step by step

### Setup

* git clone <repo>
* git remote add -f <remote-name> <other-repo>
* git subtree add --prefix <folder> <remote-name> master --squash
  * omit --squash for full history

### Update main

* git fetch <remote-name> master
* git subtree pull --prefix <folder> <remote-name> master --squash

### Update subtree

* git subtree push --prefix=<folder> <remote-name> master
